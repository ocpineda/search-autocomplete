<?php 
/*
This is used in the global nav:
- htdocs/assets/shared/ssi/global_nav.php
*/

//Below are the Global navigation auto-complete values
include 'global-search-array.php'; 
?>

var searchdata = [ 
<?php
// Convert the php array to js to be used in the jquery autocomplete function
$count = 0;
foreach ($searchdata as $a) {
	$count++;
	echo "{text:'". $a['text'] ."', desc:'". $a['desc'] ."', url:'". $a['url'] ."'}";
	if($count !== count($searchdata)) echo ",";
}
?>
];