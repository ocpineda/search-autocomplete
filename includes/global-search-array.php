<?php 
/*
Popular search keywords, descriptions and their destination urls. 
This populates the search hint in the global nav (converted to a js var) and the search landing page:
- htdocs/assets/shared/ssi/global-search-data.php 
- htdocs/search/ssi/custom-results.php
*/
$searchdata = array( 
	array(
		'text' => 'Bookstore', 
		'desc' => 'Apparel, Textbooks, Gifts and Supplies', 
		'url'=>'http://www.loyolano.bkstr.com'), 
	array(
		'text' =>'Human Resources', 
		'desc' => 'Employee Schedules, Services, Policies, Benefits', 
		'url'=>'http://www.loyno.edu/human.resources/'),
	array(
		'text' => 'Exam Schedule', 
		'desc' => 'Academic calendars, class and exam final exam schedules', 
		'url'=>'http://academicaffairs.loyno.edu/records/academic-calendars/'),
	array(
		'text' => 'Study Abroad', 
		'desc' => 'Opportunities to study in over 40 countries', 
		'url'=>'http://studyabroad.loyno.edu/'),
	array(
		'text' => 'Tuition and Fees', 
		'desc' => 'Undergraduate, Graduate, Law, and Evening tuition and fees', 
		'url'=>'http://apply.loyno.edu/undergraduate-tuition'),
	array(
		'text' => 'Athletics', 
		'desc' => 'Sports complex, recreation, sports and fitness programs for students, faculty, staff, and alumni', 
		'url'=>'http://studentaffairs.loyno.edu/athletics-wellness'),
	array(
		'text' => 'EMPLOYOLA', 
		'desc' => 'Job board, recruiters visiting campus, career fairs, workshops, and professional development events', 
		'url'=>'http://studentaffairs.loyno.edu/careers/students/employola'),
	array(
		'text' => 'Employment', 
		'desc' => 'Faculty, staff, and student job opportunities', 
		'url'=>'http://www.loyno.edu/human.resources/employment/'),
	array(
		'text' => 'Campus Map', 
		'desc' => 'Campus map and directions', 
		'url'=>'http://apply.loyno.edu/maps-directions'),
	array(
		'text' => 'Scholarships and Financial Aid', 
		'desc' => 'Office hours, financial resources, and forms', 
		'url'=>'http://www.loyno.edu/financialaid/'),
	array(
		'text' => 'Dining Services', 
		'desc' => 'Meal plans, Wolfbucks, menus and dining hours', 
		'url'=>'http://studentaffairs.loyno.edu/residential-life/campus-dining'),
	array(
		'text' => 'Calendar', 
		'desc' => 'All university events, holidays, deadlines, and important academic dates', 
		'url'=>'http://studentaffairs.loyno.edu/residential-life/campus-dining'),
	array(
		'text' => 'Orientation', 
		'desc' => 'New student orientation dates, schedule, and details', 
		'url'=>'http://studentaffairs.loyno.edu/orientation'),
	array(
		'text' => 'Service Learning', 
		'desc' => 'Community-based learning experiences in academic courses and programs of study', 
		'url'=>'http://www.loyno.edu/servicelearning/'),
	array(
		'text' => 'Commencement', 
		'desc' => 'Ceremony schedule, preparations, and details', 
		'url'=>'http://www.loyno.edu/commencement/'),
	array(
		'text' => 'Career Services', 
		'desc' => 'Student assistance in discovering their strengths and pursuing a plan', 
		'url'=>'http://studentaffairs.loyno.edu/careers'),
	array(
		'text' => 'Monroe Library', 
		'desc' => 'Research guides, books, music and videos', 
		'url'=>'http://library.loyno.edu'),
	array(
		'text' => 'Law Careers', 
		'desc' => 'Law student and alumni professional development', 
		'url'=>'http://www.loyno.edu/lawcareers'),
	array(
		'text' => 'Faculty Handbook', 
		'desc' => 'Status, rights, and responsibilities Loyola faculty', 
		'url'=>'http://academicaffairs.loyno.edu/faculty-handbook'),
	array(
		'text' => 'Credit Union', 
		'desc' => 'Tulane-Loyola federal credit union', 
		'url'=>'http://www.tulane-loyolafcu.com/portal/page/portal/Tulane%20Loyola/Home%20Page'),
	array(
		'text' => 'ESL', 
		'desc' => 'English as a second language program', 
		'url'=>'http://www.loyno.edu/cie/liep'),
	array(
		'text' => 'Community Engagement', 
		'desc' => 'Service Learning, Carnegie Foundation, civic engagement', 
		'url'=>'http://www.loyno.edu/jump/about/loyola-at-a-glance/community-engagement.php'),
	array(
		'text' => 'Nursing', 
		'desc' => 'School of Nursing, Bachelor of Science in Nursing, Master of Science in Nursing, health care systems management, adult nurse practictioner, family nurse practitioner, Doctor of Nursing Practice', 
		'url'=>'http://css.loyno.edu/nursing'),
	array(
		'text' => 'Law Records', 
		'desc' => 'Important dates, exam policies, class schedules', 
		'url'=>'http://law.loyno.edu/law-records'),
	array(
		'text' => 'Housing', 
		'desc' => 'Provide a safe and comfortable environment, Biever Hall, Buddig Hall, Cabra Hall, Carrollton Hall', 
		'url'=>'http://studentaffairs.loyno.edu/residential-life/residence-halls'),
	array(
		'text' => 'Jobs', 
		'desc' => 'Faculty, staff, and student job opportunities', 
		'url'=>'http://finance.loyno.edu/human-resources/employment/'),
	array(
		'text' => 'Residential Life', 
		'desc' => 'Provide a safe and comfortable environment, Biever Hall, Buddig Hall, Cabra Hall, Carrollton Hall', 
		'url'=>'http://studentaffairs.loyno.edu/residential-life'),
	array(
		'text' => 'LORA', 
		'desc' => 'Register for classes, view grades and transcripts, view account summaries and billing statements, pay tuition, change mailing addresses, add non-Loyola e-mail addresses, submit parking decal request and submit your personal evacuation plan', 
		'url'=>'https://lorasec.loyno.edu/'),
	array(
		'text' => 'Graduation', 
		'desc' => 'Ceremony schedule, preparations, and details', 
		'url'=>'http://www.loyno.edu/commencement/'),
	array(
		'text' => 'Financial Affairs', 
		'desc' => 'Accounting, accounts payable, reimbursements, check requests and invoices, department transfers', 
		'url'=>'http://finance.loyno.edu/financial-affairs'),
	array(
		'text' => 'Summer Session', 
		'desc' => 'Earn college credits at reduced tuition rates. On-campus and online summer courses available', 
		'url'=>'http://loyno.edu/professional-studies/summer/'),
	array(
		'text' => 'Law Library', 
		'desc' => 'Online catalog, research guides, reference assistance', 
		'url'=>'http://law.loyno.edu/library'),
	array(
		'text' => 'Psychology', 
		'desc' => 'Bachelor of Science in Psychology, Pre-Health Psychology, minor in psychology', 
		'url'=>'http://chn.loyno.edu/psychology'),
	array(
		'text' => 'Advising', 
		'desc' => 'Career planning, faculty advisor, planning coursework', 
		'url'=>'http://academicaffairs.loyno.edu/advising'),
	array(
		'text' => 'Post Office', 
		'desc' => 'Mail services, stamps, international mailing products', 
		'url'=>'http://finance.loyno.edu/post-office'),
	array(
		'text' => 'Common Curriculum', 
		'desc' => 'Core courses, philosophy, religious studies, literature, history, the sciences, and the arts', 
		'url'=>'http://academicaffairs.loyno.edu/common-curriculum'),
	array(
		'text' => 'Skills Curriculum', 
		'desc' => 'Experiential learning, externships, practical law skills, trial practice, wills, pleadings, bankruptcy claims', 
		'url'=>'http://law.loyno.edu/skills-curriculum'),
	array(
		'text' => 'Admissions Application', 
		'desc' => 'We are currently accepting applications for admission and scholarship consideration on a rolling basis.', 
		'url'=>'https://secure.loyno.edu/apply/application/'),
	array(
		'text' => 'Visit', 
		'desc' => 'Campus tours run Monday through Friday at 11:30 a.m. and 3:30 p.m., excluding University holidays and closures.', 
		'url'=>'http://apply.loyno.edu/visit-loyola'),
	array(
		'text' => 'Apply', 
		'desc' => 'We are currently accepting applications for admission and scholarship consideration on a rolling basis.', 
		'url'=>'https://secure.loyno.edu/apply/application/'),
);
?>