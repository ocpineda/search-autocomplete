$(document).ready(function(){
	// placeholder text
	var placeholder_text = "Search Loyola";
	$("#search-input").val(placeholder_text);
	$('#global-search-form .search-icon').click(function() {
		// clear placeholder text if button is clicked
		var input = $("#search-input");
	  if (input.val() == placeholder_text) {
	    input.val("");
	  }
	});
	$("#search-input").focus(function() {
	  var input = $(this);
	  if (input.val() == placeholder_text) {
	    input.val("");
	  }
	}).blur(function() {
	  var input = $(this);
	  if (input.val() == "" || input.val() == placeholder_text) {
	    input.val(placeholder_text);
	  }
	}).blur();

	// autocomplete
	$("#search-input").autocomplete(searchdata, {
		width: "180",
		matchContains: true,
		minChars: 1, 
    selectFirst: false,
	  formatItem: function(item) {
	    return item.text + "<div class='desc'>" + item.desc + "</div>";
	  },
	  formatResult: function(item) {
			return item.text;
		}
	}).result(function(event, item) {
	  if(item.url) {
	  	location.href = item.url;
	  }
	});
});